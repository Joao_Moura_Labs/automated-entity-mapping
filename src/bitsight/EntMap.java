package bitsight;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EntMap {

    Map<String, String> entmap;
    final static String[] VALIDTYPES = { // ?s ref:type validuri
    //"http://dbpedia.org/ontology/Person", "http://xmlns.com/foaf/0.1/Person", //
    //"http://dbpedia.org/ontology/Place", //
            "http://dbpedia.org/ontology/Company", //
            "http://dbpedia.org/ontology/Organisation", //
            "http://dbpedia.org/resource/Public_company", //
    //      "http://dbpedia.org/ontology/ProgrammingLanguage",
    //      "http://dbpedia.org/ontology/TelevisionShow",
    //      "http://dbpedia.org/ontology/PoliticalParty",
    //      "http://dbpedia.org/ontology/Software",
    //      "http://dbpedia.org/ontology/Film",
    //      "http://dbpedia.org/ontology/Work",
    //      "http://dbpedia.org/ontology/Event"
    };

    private static String[] ntTypes = {"dbpedia/types/instance_types_en.nt" //
    //, "Dumps/Types/instance_types_en_uris_pt.nt" //
    //, "Dumps/Types/instance_types_pt.nt" //
    };
    private static String[] ntRedirects = {"dbpedia/redirects/redirects_en.nt" //
    //, "Dumps/Redirects/redirects_pt.nt" //
    };
    private static String[] ntProperties = {//"dbpedia/properties/mappingbased_properties_en.nt" //
    //, "Dumps/Properties/mappingbased_properties_en_uris_pt.nt" //
    //, "Dumps/Properties/mappingbased_properties_pt.nt" //
    };
    private static String[] ntDisambiguations = {"dbpedia/disambiguates/disambiguations_en.nt" //
    //, "Dumps/Disambiguates/disambiguations_en_uris_pt.nt" //
    //, "Dumps/Disambiguates/disambiguations_pt.nt" //
    };

    private static String serType = "dbpedia/types.ser";
    private static String serRedirects = "dbpedia/redirects.ser";
    private static String serProperties = "dbpedia/properties.ser";
    private static String serDisambiguations = "dbpedia/disambiguations.ser";

    public EntMap() {
        this.entmap = EntMap.getTypeMap(EntMap.VALIDTYPES);
    }

    public static Map<String, String> getTypeMap(final String[] validUris) {
        final long[] t = new long[2];
        t[0] = System.currentTimeMillis();

        Map<String, String> map = null;
        final File f = new File(EntMap.serType);
        if (f.exists()) {
            // Get set from serializable file
            System.err.print("Loading DBPedia Type Map from serialized file ... ");
            try (final FileInputStream fis = new FileInputStream(f); final ObjectInputStream ois = new ObjectInputStream(fis)) {
                map = (Map<String, String>) ois.readObject();
            }
            catch (final Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            // Read from file
            System.err.print("Loading DBPedia Type Map from nt files ... ");
            map = new HashMap<String, String>();

            final List<String> valid = new ArrayList<String>();
            for (final String currUri : validUris)
                valid.add(currUri);

            for (final String currFile : EntMap.ntTypes) {
                final File fnt = new File(currFile);
                try (BufferedReader csv = new BufferedReader(new FileReader(fnt))) {
                    String line = "";
                    while (( line = csv.readLine() ) != null)
                        if (line.charAt(0) != '#') {
                            final String[] curr = EntMap.convertUnicode(line).split(" ");

                            final String s = curr[0].substring(1, curr[0].length() - 1);
                            final String p = curr[1].substring(1, curr[1].length() - 1);
                            final String o = curr[2].substring(1, curr[2].length() - 1);
                            // Filter for specific types indicated in valid list
                            if (valid.contains(o))
                                map.put(s, o);
                        }
                }
                catch (final Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            // Create serializable file
            try (FileOutputStream fos = new FileOutputStream(f); final ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(map);
            }
            catch (final Exception e) {
                e.printStackTrace();
            }
        }
        t[1] = System.currentTimeMillis();
        System.err.println("done [" + ( t[1] - t[0] ) / 1000.0 + " sec]");

        return map;
    }

    public static Map<String, List<String>> getProductsMap(final Map<String, String> mapType) {
        final long[] t = new long[2];
        t[0] = System.currentTimeMillis();

        Map<String, List<String>> map = null;
        final File f = new File(EntMap.serProperties);
        if (f.exists()) {
            // Get set from serializable file
            System.err.print("Loading DBPedia Properties Map from serialized file ... ");
            try (final FileInputStream fis = new FileInputStream(f); final ObjectInputStream ois = new ObjectInputStream(fis)) {
                map = (Map<String, List<String>>) ois.readObject();
            }
            catch (ClassNotFoundException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            // Read from file
            System.err.print("Loading DBPedia Properties Map from nt files ... ");
            map = new HashMap<String, List<String>>();

            for (final String currFile : EntMap.ntProperties) {
                final File fnt = new File(currFile);
                try (final BufferedReader csv = new BufferedReader(new FileReader(fnt))) {

                    String line = "";
                    while (( line = csv.readLine() ) != null)
                        if (line.charAt(0) != '#') {
                            final String[] curr = EntMap.convertUnicode(line).split(" ");

                            final String s = curr[0].substring(1, curr[0].length() - 1);
                            final String p = curr[1].substring(1, curr[1].length() - 1);
                            final String o = curr[2].substring(1, curr[2].length() - 1);

                            // Limit the map to products of valid types
                            if (p.equals("http://dbpedia.org/ontology/product") && ( mapType == null || mapType.containsKey(s) )) {
                                if (!map.containsKey(o))
                                    map.put(o, new ArrayList());
                                map.get(o).add(s);
                            }
                        }
                }
                catch (final IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            //TODO to be removed, debug stuff
            // Show products that share a company
            /*for (Entry<String, List<String>> e : map.entrySet()) {
                if(e.getValue().size() > 1)
                    System.out.println(e.getKey());
            }*/

            // Create serializable file
            try (final FileOutputStream fos = new FileOutputStream(f);
                    final ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(map);
            }
            catch (final Exception e) {
                e.printStackTrace();
            }
        }
        t[1] = System.currentTimeMillis();
        System.err.println("done [" + ( t[1] - t[0] ) / 1000.0 + " sec]");

        return map;
    }

    public static Map<String, List<String>> getDisambiguateMap(final Map<String, String> mapType,
            final Map<String, List<String>> mapProduct) {
        final long[] t = new long[2];
        t[0] = System.currentTimeMillis();

        Map<String, List<String>> map = null;
        final File f = new File(EntMap.serDisambiguations);
        if (f.exists()) {
            // Get set from serializable file
            System.err.print("Loading DBPedia Disambiguate Map from serialized file ... ");
            try (final FileInputStream fis = new FileInputStream(f); final ObjectInputStream ois = new ObjectInputStream(fis)) {
                map = (Map<String, List<String>>) ois.readObject();
            }
            catch (ClassNotFoundException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            // Read from file
            System.err.print("Loading DBPedia Disambiguate Map from nt files ... ");
            map = new HashMap<String, List<String>>();

            for (final String currFile : EntMap.ntDisambiguations) {
                final File fnt = new File(currFile);
                try (final BufferedReader csv = new BufferedReader(new FileReader(fnt))) {

                    String line = "";
                    while (( line = csv.readLine() ) != null)
                        if (line.charAt(0) != '#') {
                            final String[] curr = EntMap.convertUnicode(line).split(" ");

                            final String s = curr[0].substring(1, curr[0].length() - 1);
                            final String p = curr[1].substring(1, curr[1].length() - 1);
                            final String o = curr[2].substring(1, curr[2].length() - 1);

                            // Limit to ambiguous list to valid types and products of valid types
                            if (p.equals("http://dbpedia.org/ontology/wikiPageDisambiguates")
                                    && ( ( mapType == null || mapType.containsKey(o) ) || ( mapProduct == null || mapProduct
                                            .containsKey(o) ) )) {
                                if (!map.containsKey(s))
                                    map.put(s, new ArrayList<String>());
                                if (!map.get(s).contains(o))
                                    map.get(s).add(o);
                            }
                        }
                }
                catch (final IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            // Create serializable file
            try {
                final FileOutputStream fos = new FileOutputStream(f);

                final ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(map);
                oos.close();
            }
            catch (final Exception e) {
                e.printStackTrace();
            }
        }
        t[1] = System.currentTimeMillis();
        System.err.println("done [" + ( t[1] - t[0] ) / 1000.0 + " sec]");

        return map;
    }

    public static Map<String, String> getRedirectMap(final Map<String, String> mapType,
            final Map<String, List<String>> mapProduct, final Map<String, List<String>> mapDesambig) {
        final long[] t = new long[2];
        t[0] = System.currentTimeMillis();

        Map<String, String> map = null;
        final File f = new File(EntMap.serRedirects);
        if (f.exists()) {
            // Get set from serializable file
            System.err.print("Loading DBPedia Redirect Map from serialized file ... ");
            try (final FileInputStream fis = new FileInputStream(f); final ObjectInputStream ois = new ObjectInputStream(fis)) {
                map = (Map<String, String>) ois.readObject();
            }
            catch (ClassNotFoundException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            // Read from file
            System.err.print("Loading DBPedia Redirect Map from nt files ... ");
            map = new HashMap<String, String>();

            for (final String currFile : EntMap.ntRedirects) {
                final File fnt = new File(currFile);
                try (final BufferedReader csv = new BufferedReader(new FileReader(fnt))) {

                    String line = "";
                    while (( line = csv.readLine() ) != null)
                        if (line.charAt(0) != '#') {
                            final String[] curr = EntMap.convertUnicode(line).split(" ");

                            final String s = curr[0].substring(1, curr[0].length() - 1);
                            final String p = curr[1].substring(1, curr[1].length() - 1);
                            final String o = curr[2].substring(1, curr[2].length() - 1);

                            //Limit redirects to valid types or products of valid types or a valid ambiguous uri
                            if (( mapType == null || mapType.containsKey(o) )
                                    || ( mapProduct == null || mapProduct.containsKey(o) )
                                    || ( mapDesambig == null || mapDesambig.containsKey(o) ))
                                map.put(s, o);
                        }
                }
                catch (final IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            // Create serializable file
            try {
                final FileOutputStream fos = new FileOutputStream(f);

                final ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(map);
                oos.close();
            }
            catch (final Exception e) {
                e.printStackTrace();
            }
        }
        t[1] = System.currentTimeMillis();
        System.err.println("done [" + ( t[1] - t[0] ) / 1000.0 + " sec]");

        return map;
    }

    private final static Pattern p = Pattern.compile("(\\\\u.{4})");

    public static String convertUnicode(final String line) {
        String res = line;
        final Matcher m = EntMap.p.matcher(res); // get a matcher object
        while (m.find()) {
            final String code = m.group();
            res = res.replace(code, "" + (char) Integer.parseInt(code.substring(2), 16));
        }

        final Pattern p2 = Pattern.compile("(%[0-9a-fA-F]{2}%[0-9a-fA-F]{2})");
        final Matcher m2 = p2.matcher(res); // get a matcher object
        while (m2.find()) {
            final String code = m2.group();
            try {
                res = res.replace(code, URLDecoder.decode(code, "UTF-8"));
            }
            catch (final UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                //              e.printStackTrace();
            }
        }

        return res;
    }

}
