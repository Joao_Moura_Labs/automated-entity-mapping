package bitsight;

import java.util.List;
import java.util.Map;

public class Main {

    public static void main(final String[] args) {
        final Map<String, String> mapType = EntMap.getTypeMap(EntMap.VALIDTYPES);
        final Map<String, List<String>> mapProduct = EntMap.getProductsMap(mapType);
        final Map<String, List<String>> mapDesambig = EntMap.getDisambiguateMap(mapType, mapProduct);
        final Map<String, String> mapRedirect = EntMap.getRedirectMap(mapType, mapProduct, mapDesambig);

        // Do stuff
    }
}
